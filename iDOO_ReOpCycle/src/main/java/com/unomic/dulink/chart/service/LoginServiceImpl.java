package com.unomic.dulink.chart.service;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.unomic.dulink.chart.domain.UserVo;

@Service
@Repository
public class LoginServiceImpl implements LoginService {
	
	private final static String namespace = "com.unomic.dulink.login.";
	
	private static final Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);

	@Inject
	SqlSession sqlSessionTemplate;

	@Override
	public String loginCheck(UserVo vo, HttpSession session) throws Exception {
		String name = (String)sqlSessionTemplate.selectOne("loginCheck", vo);
        return name;
	}

	@Override
	public boolean logout(HttpSession session) throws Exception {
		// TODO 자동 생성된 메소드 스텁
		return false;
	}

}
