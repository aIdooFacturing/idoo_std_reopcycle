const width = window.innerWidth
const height = window.innerHeight

const targetWidth = 3840;
const targetHeight = 2160;

const originWidth = window.innerWidth;
const originHeight = window.innerHeight;

let contentWidth = originWidth;
let contentHeight = targetHeight / ( targetWidth / originWidth );

const getElSize = n => {
    return contentWidth/(targetWidth/n);
};

const setElSize = n => {
    return Math.floor(targetWidth / (contentWidth / n));
};

const screen_ratio = getElSize(240);

if(originHeight / screen_ratio < 9){
    contentWidth = targetWidth / ( targetHeight / originHeight )
    contentHeight = originHeight;
};

const marginWidth = ( originWidth - contentWidth ) / 2;
const marginHeight = ( originHeight - contentHeight ) / 2;

$(()=>{
    createHeader()
    createCorver()
    chkLogin()
})

const createCorver = () => {
    const corver = document.createElement("div")
    corver.setAttribute("id", "corver")
    corver.style.cssText =
        "position : absolute;" +
        "width : " + width + "px;" +
        "height : " + height + "px;" +
        "background-color : rgba(0, 0, 0, 0);" +
        "transition : 1s;" +
        "z-index : -1;";

    $("body").prepend(corver)
}

const showCorver = () =>{
    $("#corver").css({
        "background-color" : "rgba(0, 0, 0, 0.7)",
        "z-index": 2
    })
}

const hideCorver = () => {
    $("#corver").css({
        backgroundColor : "rgba(0, 0, 0, 0)"
    })

    setTimeout(()=>{
        $("#corver").css("z-index", -1)
    }, 1000)
}

const chkLogin = () =>{
    if(location.href[location.href.length-1] != "/"){
        const span = document.createElement("span")
        span.append(document.createTextNode("김부광"))

        span.style.cssText =
            `
                color : white
                ; position : absolute
                ; font-size : ${getElSize(50)}px
                
            `

        $("#header").append(span)

        $(span).css({
            "top" : $("#header").height() / 2 - ($(span).height() / 2),
            "left" : getElSize(400)

        })

        const logout = document.createElement("img")
        logout.setAttribute("id", "logout")
        logout.setAttribute("src", "https://www.digitaltwincloud.com/EA/assets/imgs/default/logout.png")

        logout.style.cssText =
            `
                cursor : pointer
                ; left : ${$(span).offset().left + $(span).width() +  getElSize(20)}px
                ; top  : ${$("#header").height() / 2 - getElSize(25)}px
                ; height : ${getElSize(50)}px
                ; position : absolute
            `

        $(logout).click(fn_logout)
        $("#header").append(logout)
    }
}

const fn_logout = () =>{
    location.href = "/"
}

const createHeader = () =>{
    const div = document.createElement("div")
    div.setAttribute("id", "header")

    div.style.cssText =
        `background-color : black
        ; height : ${height * 0.06}px`;


    const logo = document.createElement("img")
    logo.setAttribute("src", "https://www.digitaltwincloud.com/assets/imgs/default/logo.png")
    logo.setAttribute("id", "logo")

    logo.style.cssText =
        `
           margin-top : ${$(div).height() / 2 - $(div).height() * 0.25}px
           ;margin-left : ${getElSize(20)}px
           ;height : ${$(div).height() * 0.5}px
        `

    div.append(logo)

    const bk_logo = document.createElement("img")
    bk_logo.setAttribute("src", "http://www.bukwangpi.com/img_up/shop_pds/bukwang/design/img/logo.png")
    bk_logo.setAttribute("onclick", "popIndex.do")

    bk_logo.style.cssText = 
        `
           height : ${$(div).height() * 0.6}px
           ; margin-top : ${$(div).height() / 2 - $(div).height() * 0.3}px
           ; margin-right : ${getElSize(20)}px
           ; border-radius : ${getElSize(10)}px
           ; float : right
        
           ; background-color : white
        `
    div.append(bk_logo)

    $("body").prepend(div)
}

const setSelectFormat = () => {
    [].slice.call( document.querySelectorAll( '.unos_select' ) ).forEach( function(el) {
        new SelectFx(el);
    });

    $(".unos_select").css({
        "font-size" : getElSize(60) + "px",
        "max-width": getElSize(500) + "px"
    })

    $(".unos_select > span").css({
        "border-bottom" : getElSize(3) + "px solid",
        "border-color" : "#D70000"
    });
}

const addZero = n =>{
    if(n.length == 1){
        n = "0" + n
    }
    return n
}

const getNow = () =>{
    const date = new Date();
    const year = date.getFullYear();
    const month = addZero(String(date.getMonth() + 1))
    const day = addZero(String(date.getDate()))

    const hour = addZero(String(date.getHours()))
    const minute = addZero(String(date.getMinutes()))
    const second = addZero(String(date.getSeconds()))

    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second
}

const setTableFormat = () => {
    $(".mainTable").css({
        "border-spacing" : getElSize(5) + "px",
        "border-radius" : getElSize(50) + "px"
    })

    $(".mainTable td").css({
        "color" : "black",
        "text-align" : "center",
        "padding" : getElSize(30) + "px",
        "font-size" : getElSize(40) + "px"
    })

    $(".mainTable thead td").css({
        "background-color" : "rgba(255,0,0,0.6)",
        "border-radius" : getElSize(20) + "px",
        "padding" : getElSize(20) + "px",
        "color" : "white",
    })

    $(".mainTable td, .searchTable td").css({
        "border-bottom" : getElSize(3) + "px solid #E1DBD9"
    })

    const $table_hover = Rx.Observable.fromEvent($(".mainTable tr").not($(".mainTable tr:nth(0)")), "mouseenter")
    $table_hover.map(e =>
        e.currentTarget
    ).subscribe(e=>
        $(e).css("background-color" , "rgba(0,0,0,0.1)")
    )

    const $table_out= Rx.Observable.fromEvent($(".mainTable tr").not($(".mainTable tr:nth(0)")), "mouseleave")
    $table_out.map(e =>
        e.currentTarget
    ).subscribe(e=>
        $(e).css("background-color" , "rgba(0,0,0,0)")
    )
}


const setPaging = (div, totalPage, func) => {
    $.jqPaginator('#' + div , {
        totalPages: totalPage,
        visiblePages: 10,
        currentPage: 1,
        onPageChange: function (num, type) {
            func(num)
        }
    });
}