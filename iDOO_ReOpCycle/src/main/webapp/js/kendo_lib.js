var windowTemplate, delMsg;
		
$(function(){
	windowTemplate = kendo.template($("#windowTemplate").html());
	delMsg = $("#window").kendoWindow({
	    title: chk_del,
	    visible: false, //the window will not appear before its .open method is called
	    width: getElSize(1000) + "px",
	    height: getElSize(250) + "px",
	}).data("kendoWindow");
	
	
});

function del_btn_evt2(e, el, cd){
	grid = $("#grid").data("kendoGrid");
	e.preventDefault();
	var data = grid.dataSource.data()[0]; //get the row data so it can be referred later
	
	
	delMsg.content(windowTemplate(data)); //send the row data object to the template and render it
	delMsg.center().open();
	
	
	
	var grid = $("#grid").data("kendoGrid");
	 
    $("#yesButton").click(function(){
    	eval(cd)
        grid.dataSource.remove(data)  //prepare a "destroy" request
        grid.dataSource.sync()  //actually send the request (might be ommited if the autoSync option is enabled in the dataSource)
        delMsg.close();
        $(".k-button").css({
			"padding" : getElSize(10)
		})
    })
    $("#noButton").click(function(){
    	delMsg.close();
    }) 
}

function del_btn_evt(e, el, cd){
	e.preventDefault();
	var tr = $(e.target).closest("tr");
	//var data = el.dataItem(tr); //get the row data so it can be referred later
	
	
//	console.log(e)
//	console.log(el)
//	console.log(data)
//	console.log(delMsg)
//	delMsg.content(windowTemplate(data)); //send the row data object to the template and render it
//	delMsg.center().open();
	
	
	
	var grid = $("#grid").data("kendoGrid");
    var dataItem = grid.dataItem(tr);
	var id = dataItem.id 
	 
	del_id = dataItem.dvcId;
	
	if(confirm(chk_del)){
		eval(cd)
        grid.dataSource.remove(dataItem) 
	};
	
    $("#yesButton").click(function(){
    	eval(cd)
        grid.dataSource.remove(data)  //prepare a "destroy" request
       // grid.dataSource.sync()  //actually send the request (might be ommited if the autoSync option is enabled in the dataSource)
        delMsg.close();
//        $(".k-button").css({
//			"padding" : getElSize(10)
//		})
    })
    $("#noButton").click(function(){
    	delMsg.close();
    });
    
}

var checkedIds = {};
function selectRow() {
    var checked = this.checked,
    row = $(this).closest("tr"),
    grid = $("#grid").data("kendoGrid"),
    dataItem = grid.dataItem(row);

    checkedIds[dataItem.id] = checked;
    if (checked) {
        //-select the row
        row.addClass("k-state-selected");
        } else {
        //-remove selection
        row.removeClass("k-state-selected");
    }
}