<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>
body{
	background: #484848;
	margin: 0;
	color: white;
	width: 100%;
	height: 100%;
	position: absolute;
}
#header{
	width: 100%;
	height: 15%;
}
#backBtn{
	background: linear-gradient( darkslateblue,#7112FF);
	border-radius: 5%;
	width: 20%;
	height: 100%;
	display: table;
	float: left;
	
}
#backBtn span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 300%;
}
#title{
	background: linear-gradient( #000000,#484848);
	width: 80%;
	height: 100%;
	display: table;
	float: left;
}
#title span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 500%;
}
#aside{
	background: green;
	width: 100%;
	height: 15%;
	display: table;
	background: darkorchid;
	color:indigo;
}
#aside span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 400%;
}
#content{
	width: 100%;
	height: 70%;
	background: #242424;
	overflow: auto;
}

#addDevice{
	background: red;
}

#jobList{
	height: 90%;
	overflow: auto;
}
</style>

<script>
	//이벤트 메세지 값 
	var evtMsg;
	
	//장비 dvcId
	var dvcId;
	
	//장비 이름 선택값
	var name;

	//사원번호 선택값
	var nm;
	
	//사원번호 코드
	var empCd;

	//공정 ty
	var ty;
	
	$(function(){
		getTable();
		
		//focus TEXT 맞추기
		var chk_short = true;
		
		$(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#empCd").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#empCd").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
	})
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	//enter key event
	function enterEvt(event) {
		if(event.keyCode == 13){
			getTable()
		}
	}
	
	
	//사원 바코드를 입력해주세요
	function alarmMsg(){
		clearTimeout(evtMsg)
		return evtMsg = setTimeout(function() {$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>사원 바코드를 입력해주세요</marquee></span>")}, 10000)
	}

	//소재 바코드를 입력해주세요
	function alarmMsg2(){
		clearTimeout(evtMsg)
		return evtMsg = setTimeout(function() {$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>소재 바코드를 입력해주세요</marquee></span>")}, 10000)
	}
	//작업리스트 조회 후 장비 선택했을시
	function dvcSelect(e){
		console.log(e)
		console.log(e.closest("tr"))
		row = e.closest("tr");
		
		var dvcId = row.cells[1].getAttribute("class");
		var ty = row.cells[1].getAttribute("name");
		var name = encodeURIComponent(row.cells[1].innerHTML);
		location.href="${ctxPath}/pop/selectStartJob.do?nm="+nm+"&name="+name+"&empCd="+empCd+"&dvcId="+dvcId+"&ty="+ty;
		
/* 		$("#searchText").html('<div align="center" style="height: 10%"> 바코드 : <input type="text" id="empCd" onkeyup="enterEvt(event)" style="vertical-align: middle;"></div>')
		$("#jobList").empty()
		$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText' style='color:blue;'>소재 바코드를 입력해주세요.</marquee></span>") */

	}
	function getTable(){
		if($("#empCd").val()==0){
			return;
		}
		$("#empCd").focus();
		$("#empCd").select();
		
		empCd=$("#empCd").val();
		
		//앞에 4자리는 바코드 구분 현재 1111이면 사원증
		if(empCd.substring(0,4)!="1111"){
			$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>바코드를 다시 확인해 주세요</marquee></span>")
			$("#jobList").empty()
			alarmMsg();
			return;
		}
		
		empCd = empCd.substring(4,empCd.length);
		$.showLoading(); 
		
		var url = "${ctxPath}/pop/getStartJobList.do";
		
		
		
		var param = "empCd=" + empCd+
 					"&sDate=" + moment().subtract(6,"month").format("YYYY-MM-DD") +
					"&eDate=" + moment().format("YYYY-MM-DD") ;  
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				var table = "<table id='test' style='width:100%;text-align:center; font-size:200%; border-spacing: 0 20;'><tr><td colspan='4' style='text-align:right;'><input type='button' id='addDevice' value='장비추가' onclick='location.href=\"${ctxPath}/pop/addDevice.do?empCd="+ empCd +"\"' style='vertical-align: middle; margin-right:5%; font-size:200%; '></td></tr>"
					table += "<tr><th>작업자</th><th>장비</th><th>주/야간</th><th>선택</th></tr>"
//					table += '<tr><td colspan="6"><input type="button" value="asdas" style="vertical-align: middle; margin-left: 50%;"></td></tr>'								location.href='${ctxPath}/pop/popIndex.do'
				$(json).each(function(idx,data){
					data.name = decode(data.name);
					data.encode = data.name;
					nm = data.nm
					data.nm = decode(data.nm);
					
					if(data.workIdx==2){
						data.workIdx="주간"
					}else{
						data.workIdx="야간"
					}

					table += "<tr><td>" + data.nm + "</td>"
					table += "<td class=" + data.dvcId +" name="+ data.ty +">" + data.name + "</td>"
					table += "<td>" + data.workIdx + "</td>"
					table += "<td>" + "<input type='button' value='선택' style='font-size:100%;' onclick=dvcSelect(this)>" + "</td></tr>"
						
				});
					
				$("#jobList").empty()
				$("#jobList").append(table)
				
				clearTimeout(evtMsg)
				$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText' style='color:blue;'>작업할 장비를 선택해주세요.</marquee></span>")
				
				$.hideLoading(); 
			},error : function(request,status,error){
				if(request.responseText=="no"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>등록된 사원번호가 아닙니다.</marquee></span>")
					$("#jobList").empty()
					alarmMsg();
				}else{
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>관리자에게 문의해주세요.</marquee></span>")
					$("#jobList").empty()
					alarmMsg();
					
				}
				$.hideLoading(); 
//				 alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				   


			}
		});
		
	}
</script>

<body>
	<div id="header">
		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
			<span>BKJM</span>
		</div>
		<div id="title">
			<span>작 업 시 작</span>
		</div>
	</div>
	<div id="aside">
		<span>
			<marquee behavior=alternate scrollamount="20">
				사원 바코드를 입력해주세요
			</marquee>
		</span>
	</div>
	<div id="content">
		<div id="searchText" align="center" style="font-size: 200%; height: 10%">
			사원 번호 : <input type="text" id="empCd" onkeyup="enterEvt(event)" style="vertical-align: middle;">
		</div>
		<div id="jobList">
		</div>
	</div>
</body>
</html>